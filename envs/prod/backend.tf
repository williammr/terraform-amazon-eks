terraform {
  backend "s3" {
    bucket         = "tf-states.cloud-nation.net"
    key            = "prod.tfstate"
    region         = "us-east-1"
    dynamodb_table = "tf-states.cloud-nation.net"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.63"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.6.1"
    }
  }
}
