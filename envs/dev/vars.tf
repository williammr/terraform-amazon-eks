## Environment Configuration

variable "region" {
  description = "Default Region"
  default     = "us-east-1"
}

variable "namespace" {
  description = "Default Namespace"
  default     = "cloud-nation"
}

variable "stage" {
  description = "Development Environment"
  default     = "dev"
}

variable "name" {
  description = "Generic Name"
  default     = "dev"
}

## Kubernetes Version 1.21

variable "kubernetes_version" {
  description = "Kubernetes Version"
  default     = "1.19"
}

variable "kubernetes_cluster_name" {
  description = "Kubernetes Cluster Name"
  default     = "dev"
}

variable "instance_types" {
  description = "Kubernetes Instance Types"
  default     = ["t3.medium"]
}

variable "desired_size" {
  description = "Kubernetes Instance Desired Size"
  default     = 1
}

variable "min_size" {
  description = "Kubernetes Instance Min Size"
  default     = 1
}

variable "max_size" {
  description = "Kubernetes Instance Max Size"
  default     = 5
}

variable "ec2_ssh_key" {
  description = "Kubernetes EC2 SSH Key"
  default     = "cloud-station"
}

## Network Configuration

variable "azs" {
  description = "Availability Zones"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "route53_zone" {
  description = "Route 53 Zone"
  type        = string
  default     = "dev.cloud-nation.net."
}

variable "domain_name" {
  description = "Domain Name"
  type        = string
  default     = "dev.cloud-nation.net"
}

## Secrets Manager Configuration

variable "sm_db_name_key" {
  description = "Secrets Manager DB_NAME key"
  type        = string
  default     = "DB_NAME"
}

variable "sm_db_username_key" {
  description = "Secrets Manager DB_USERNAME key"
  type        = string
  default     = "DB_USERNAME"
}

variable "sm_db_password_key" {
  description = "Secrets Manager DB_PASSWORD key"
  type        = string
  default     = "DB_PASSWORD"
}
