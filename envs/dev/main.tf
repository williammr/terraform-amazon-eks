# network configuration
locals {
  cidr_block         = "10.0.0.0/16"
  private_cidr_block = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_cidr_block  = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  db_cidr_block      = ["10.0.7.0/24", "10.0.8.0/24", "10.0.9.0/24"]
}

# us-east-1
provider "aws" {
  region = var.region
}

# deploy vpc and eks
module "base" {
  source = "../../base"

  region                  = var.region
  namespace               = var.namespace
  stage                   = var.stage
  name                    = var.name
  vpc_name                = var.name
  azs                     = var.azs
  cidr_block              = local.cidr_block
  public_cidr_block       = local.public_cidr_block
  private_cidr_block      = local.private_cidr_block
  db_cidr_block           = local.db_cidr_block
  kubernetes_version      = var.kubernetes_version
  kubernetes_cluster_name = var.kubernetes_cluster_name
  instance_types          = var.instance_types
  desired_size            = var.desired_size
  min_size                = var.min_size
  max_size                = var.max_size
  ec2_ssh_key             = var.ec2_ssh_key
  route53_zone            = var.route53_zone
  sm_db_name_key          = var.sm_db_name_key
  sm_db_username_key      = var.sm_db_username_key
  sm_db_password_key      = var.sm_db_password_key
  domain_name             = var.domain_name
}

# Deploying the cluster 01
