terraform {
  backend "s3" {
    bucket         = "tf-states-dev.cloud-nation.net"
    key            = "dev.tfstate"
    region         = "us-east-1"
    dynamodb_table = "tf-states-dev.cloud-nation.net"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.53"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.6.1"
    }
  }
}
