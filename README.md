# Amazon EKS with Terraform

## How to connect to Amazon EKS Cluster

### dev cluster

```
$ aws eks --region us-east-1 update-kubeconfig --name dev --profile cloud-nation-development
```

### prod cluster

```
$ aws eks --region us-east-1 update-kubeconfig --name prod --profile cloud-nation-development
```

## Kustomize configuration

```
# Deploy
$ kustomize build . | kubectl apply -f -

# Delete
$ kustomize build . | kubectl delete -f -
```

## ToDo

- RBAC
- External DNS
- Network Policies
- Cert Manager
- Ingress
- Monitoring

## How to install Calico on Amazon EKS

### Resources

- https://docs.aws.amazon.com/eks/latest/userguide/calico.html

## How to test PostgreSQL

```
kubectl run -it --rm --image=jbergknoff/postgresql-client --restart=Never postgres-client -- postgres://[USERNAME]:[PASSWORD]@[DB_ENDPOINT]:5432/[DATABASE_NAME]
```
