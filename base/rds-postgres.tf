# module "label-postgres" {
#   source    = "cloudposse/label/null"
#   version   = "0.24.1"
#   namespace = var.namespace
#   stage     = var.stage
#   name      = "postgres"
#   delimiter = "-"

#   tags = {
#     "Terraformed" = true,
#     "Project"     = "AWS EKS"
#   }
# }

# module "rds-postgres" {
#   source                      = "cloudposse/rds/aws"
#   version                     = "0.36.0"
#   namespace                   = var.namespace
#   stage                       = var.stage
#   name                        = data.aws_secretsmanager_secret_version.db_name.secret_string
#   dns_zone_id                 = data.aws_route53_zone.selected.zone_id
#   host_name                   = "postgres"
#   database_name               = data.aws_secretsmanager_secret_version.db_name.secret_string
#   database_user               = data.aws_secretsmanager_secret_version.db_username.secret_string
#   database_password           = data.aws_secretsmanager_secret_version.db_password.secret_string
#   database_port               = 5432
#   multi_az                    = true
#   storage_type                = "gp2"
#   allocated_storage           = 20
#   storage_encrypted           = true
#   engine                      = "postgres"
#   engine_version              = "12.5"
#   major_engine_version        = "12.5"
#   instance_class              = "db.t2.medium"
#   db_parameter_group          = "postgres12"
#   option_group_name           = aws_db_option_group.postgres-options.name
#   publicly_accessible         = false
#   subnet_ids                  = values(module.db_private_subnets.az_subnet_ids)
#   vpc_id                      = module.vpc.vpc_id
#   auto_minor_version_upgrade  = true
#   allow_major_version_upgrade = false
#   apply_immediately           = true
#   maintenance_window          = "Mon:03:00-Mon:04:00"
#   skip_final_snapshot         = true
#   copy_tags_to_snapshot       = true
#   backup_retention_period     = 0
#   backup_window               = "22:00-03:00"
#   allowed_cidr_blocks         = [module.vpc.vpc_cidr_block]

#   context = module.label-postgres.context
# }

# resource "aws_db_option_group" "postgres-options" {
#   name                     = "postgres-12-0"
#   option_group_description = "Terraform PostgreSQL Option Group"
#   engine_name              = "postgres"
#   major_engine_version     = "12"
# }