module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"

  cluster_name                         = var.kubernetes_cluster_name
  cluster_version                      = var.kubernetes_version
  vpc_id                               = module.vpc.vpc_id
  subnets                              = concat(module.vpc.private_subnets, module.vpc.public_subnets)
  cluster_endpoint_private_access      = true
  cluster_endpoint_public_access       = true
  worker_additional_security_group_ids = [aws_security_group.all_worker_mgmt.id]
  manage_aws_auth                      = "true"
  enable_irsa                          = "true"

  worker_groups = [
    {
      name                          = "${var.kubernetes_cluster_name}-worker-group-1"
      instance_type                 = "t3.small"
      additional_userdata           = "echo test"
      asg_desired_capacity          = 1
      asg_max_size                  = 5
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]

      tags = [
        {
          key                 = "k8s.io/cluster-autoscaler/enabled"
          propagate_at_launch = "false"
          value               = "true"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/${var.kubernetes_cluster_name}"
          propagate_at_launch = "false"
          value               = "true"
        }
      ]
    },
    {
      name                          = "${var.kubernetes_cluster_name}-worker-group-2"
      instance_type                 = "t3.medium"
      additional_userdata           = "echo test"
      asg_desired_capacity          = 1
      asg_max_size                  = 5
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_two.id]

      tags = [
        {
          key                 = "k8s.io/cluster-autoscaler/enabled"
          propagate_at_launch = "false"
          value               = "true"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/${var.kubernetes_cluster_name}"
          propagate_at_launch = "false"
          value               = "true"
        }
      ]

      #map_roles TODO: define the roles
      #map_users TODO: define the users
      #map_accounts TODO: define the accounts
    },
  ]
}

resource "aws_security_group" "worker_group_mgmt_one" {
  name_prefix = "worker_group_mgmt_one"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }
}

resource "aws_security_group" "worker_group_mgmt_two" {
  name_prefix = "worker_group_mgmt_two"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22

    cidr_blocks = [
      "192.168.0.0/16",
    ]
  }
}

resource "aws_security_group" "all_worker_mgmt" {
  name_prefix = "all_worker_management"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22

    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
}
