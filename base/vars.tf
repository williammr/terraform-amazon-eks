variable "region" {
  description = "Default Region"
  type        = string
}

variable "namespace" {
  description = "Default Namespace"
  type        = string
}

variable "stage" {
  description = "Development Environment"
  type        = string
}

variable "name" {
  description = "Generic Name"
  type        = string
}

variable "vpc_name" {
  description = "VPC Name"
  type        = string
}

## Kubernetes Version ##

variable "kubernetes_version" {
  description = "Kubernetes Version"
  type        = string
}

variable "kubernetes_cluster_name" {
  description = "Kubernetes Cluster Name"
  type        = string
}

variable "instance_types" {
  description = "Kubernetes Instance Types"
  default     = []
}

variable "desired_size" {
  description = "Kubernetes Instance Desired Size"
  type        = number
}

variable "min_size" {
  description = "Kubernetes Instance Min Size"
  type        = number
}

variable "max_size" {
  description = "Kubernetes Instance Max Size"
  type        = number
}

variable "ec2_ssh_key" {
  description = "Kubernetes EC2 SSH Key"
  type        = string
}

## Network Configuration

variable "cidr_block" {
  description = "VPC CIDR Block"
  type        = string
}

variable "public_cidr_block" {
  description = "Public CIDR Block"
  type        = list(string)
}

variable "private_cidr_block" {
  description = "Private CIDR Block"
  type        = list(string)
}

variable "db_cidr_block" {
  description = "DB CIDR Block"
  type        = list(string)
}

variable "public_subnet_name" {
  description = "Public Subnet Name"
  type        = string
  default     = "public"
}

variable "private_subnet_name" {
  description = "Private Subnet Name"
  type        = string
  default     = "private"
}

variable "db_private_subnet_name" {
  description = "DB Private Subnet Name"
  type        = string
  default     = "db"
}

variable "azs" {
  description = "Availability Zones"
  type        = list(string)
}

variable "route53_zone" {
  description = "Route 53 Zone"
  type        = string
}

variable "domain_name" {
  description = "Domain Name"
  type        = string
}

# Secrets Manager

variable "sm_db_name_key" {
  description = "Secrets Manager DB_NAME key"
  type        = string
}

variable "sm_db_username_key" {
  description = "Secrets Manager DB_USERNAME key"
  type        = string
}

variable "sm_db_password_key" {
  description = "Secrets Manager DB_PASSWORD key"
  type        = string
}
