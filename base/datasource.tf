data "aws_region" "current" {}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

#data "aws_secretsmanager_secret" "db_name" {
#  name = var.sm_db_name_key
#}

#data "aws_secretsmanager_secret_version" "db_name" {
#  secret_id = data.aws_secretsmanager_secret.db_name.id
#}

#data "aws_secretsmanager_secret" "db_username" {
#  name = var.sm_db_username_key
#}

#data "aws_secretsmanager_secret_version" "db_username" {
#  secret_id = data.aws_secretsmanager_secret.db_username.id
#}

#data "aws_secretsmanager_secret" "db_password" {
#  name = var.sm_db_password_key
#}

#data "aws_secretsmanager_secret_version" "db_password" {
#  secret_id = data.aws_secretsmanager_secret.db_password.id
#}

data "aws_route53_zone" "selected" {
  name = var.route53_zone
}
