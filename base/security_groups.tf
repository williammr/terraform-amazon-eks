/* RDS MySQL Configuration 

module "rds_mysql_sg" {
  source  = "cloudposse/security-group/aws"
  version = "0.3.1"
  vpc_id  = module.vpc.vpc_id

  rules = [
    {
      type        = "ingress"
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      description = "Allow MySQL from anywhere"
    },
    {
      type        = "egress"
      from_port   = 0
      to_port     = 65535
      protocol    = "all"
      cidr_blocks = ["0.0.0.0/0"]
      description = "Allow egress to anywhere"
    }
  ]

  context = module.label-mysql.context
}
*/

# module "rds_postgres_sg" {
#   source  = "cloudposse/security-group/aws"
#   version = "0.3.1"
#   vpc_id  = module.vpc.vpc_id

#   rules = [
#     {
#       type        = "ingress"
#       from_port   = 5432
#       to_port     = 5432
#       protocol    = "tcp"
#       cidr_blocks = ["0.0.0.0/0"]
#       description = "Allow PostgreSQL from anywhere"
#     },
#     {
#       type        = "egress"
#       from_port   = 0
#       to_port     = 65535
#       protocol    = "all"
#       cidr_blocks = ["0.0.0.0/0"]
#       description = "Allow egress to anywhere"
#     }
#   ]

#   context = module.label-postgres.context
# }

#module "worker_sg" {
#  source  = "cloudposse/security-group/aws"
#  version = "0.3.1"
#  vpc_id  = module.vpc.vpc_id

#  rules = [
#    {
#      type        = "ingress"
#      from_port   = 0
#      to_port     = 65535
#      protocol    = "tcp"
#      cidr_blocks = ["0.0.0.0/0"]
#      description = "Allow Web Server from anywhere"
#    },
#    {
#      type        = "egress"
#      from_port   = 0
#      to_port     = 65535
#      protocol    = "all"
#      cidr_blocks = ["0.0.0.0/0"]
#      description = "Allow egress to anywhere"
#    }
#  ]

#  context = module.label-worker.context
#}
