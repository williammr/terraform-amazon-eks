module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name                 = var.vpc_name
  cidr                 = var.cidr_block
  azs                  = var.azs
  private_subnets      = var.private_cidr_block
  public_subnets       = var.public_cidr_block
  database_subnets     = var.db_cidr_block
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.vpc_name}" = "shared"
    "kubernetes.io/role/elb"                = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.vpc_name}" = "shared"
    "kubernetes.io/role/internal-elb"       = "1"
  }

  tags = {
    "kubernetes.io/cluster/${var.vpc_name}" = "shared"
    "terraformed"                           = "true"
  }
}
