# Amazon Secrets Manager

## AWS Secrets Manager for Dev

```
aws secretsmanager create-secret --name 'AWS_ACCOUNT_ID' --secret-string 'YOUR_AWS_ACCOUNT_ID' --description 'AWS Account Id' --profile cloud-nation-development
aws secretsmanager create-secret --name 'DB_NAME' --secret-string 'usermtmgpg' --description 'Database Name' --profile cloud-nation-development
aws secretsmanager create-secret --name 'DB_USERNAME' --secret-string 'admin101' --description 'Database Username' --profile cloud-nation-development
aws secretsmanager create-secret --name 'DB_PASSWORD' --secret-string 'password101' --description 'Database Password' --profile cloud-nation-development
aws secretsmanager create-secret --name 'DB_DIALECT_CODIMD' --secret-string 'postgres' --description 'Database Dialect' --profile cloud-nation-development
aws secretsmanager create-secret --name 'DB_ENDPOINT' --secret-string 'dummy' --description 'Database Endpoint' --profile cloud-nation-development
```

## AWS Secrets Manager for Prod

```
aws secretsmanager create-secret --name 'AWS_ACCOUNT_ID' --secret-string 'YOUR_AWS_ACCOUNT' --description 'AWS Account Id' --profile cloud-nation-production
aws secretsmanager create-secret --name 'DB_NAME_PROD' --secret-string 'usermtmgpg' --description 'Database Name' --profile cloud-nation-production
aws secretsmanager create-secret --name 'DB_USERNAME_PROD' --secret-string 'admin101' --description 'Database Username' --profile cloud-nation-production
aws secretsmanager create-secret --name 'DB_PASSWORD_PROD' --secret-string 'password101' --description 'Database Password' --profile cloud-nation-production
aws secretsmanager create-secret --name 'DB_DIALECT_CODIMD' --secret-string 'postgres' --description 'Database Dialect' --profile cloud-nation-production
aws secretsmanager create-secret --name 'DB_ENDPOINT' --secret-string 'dummy' --description 'Database Endpoint' --profile cloud-nation-production
```
