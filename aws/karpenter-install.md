# Karpenter install

## Environment variables

```
export CLUSTER_NAME=dev
export AWS_DEFAULT_REGION=us-east-1
AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output text --profile cloud-nation-development)
```

## Create the Karpenter IAM role

```
TEMPOUT=$(mktemp)
curl -fsSL https://karpenter.sh/docs/getting-started/cloudformation.yaml > $TEMPOUT \
&& aws cloudformation deploy \
  --stack-name Karpenter-${CLUSTER_NAME} \
  --template-file ${TEMPOUT} \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameter-overrides ClusterName=${CLUSTER_NAME}
  --profile cloud-nation-development
```

- TODO: get the IAM Role and IAM Policy ARNs

## Add Karpenter node role to aws-auth configmap

```
kubectl edit -n kube-system configmap/aws-auth

Add to map roles other group:

mapRoles: |
    - "groups": 
      - "system:bootstrappers" 
      - "system:nodes"
      "rolearn": "arn:aws:iam::164063389910:role/dev2021120111481575200000000e"
      "username": "system:node:{{EC2PrivateDNSName}}"
    - "groups": 
      - "system:bootstrappers"
      - "system:nodes"
      "rolearn": "arn:aws:iam::164063389910:role/KarpenterNodeRole-dev"
      "username": "system:node:{{EC2PrivateDNSName}}"

configmap/aws-auth edited
```

- 164063389910 is a fake account number

## Create the KarpenterController IAM Role

### Set OIDC_PROVIDER

```
OIDC_PROVIDER=$(aws eks describe-cluster --name dev --query "cluster.identity.oidc.issuer" --output text --profile cloud-nation-development | sed -e "s/^https:\/\///")
```

### Assume role file

```
read -r -d '' TRUST_RELATIONSHIP <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::${AWS_ACCOUNT_ID}:oidc-provider/${OIDC_PROVIDER}"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "${OIDC_PROVIDER}:sub": "system:serviceaccount:karpenter:karpenter"
        }
      }
    }
  ]
}
EOF
echo "${TRUST_RELATIONSHIP}" > trust.json
```

### Create the role

```
aws iam create-role --role-name karpenter --assume-role-policy-document file://trust.json --description "Karpenter IAM Role" --profile cloud-nation-development
```

### Attach IAM Policy to the role

```
aws iam attach-role-policy --role-name karpenter --policy-arn=arn:aws:iam::164063389910:policy/KarpenterControllerPolicy-dev --profile cloud-nation-development
```

### Create service account

```
kubectl create namespace karpenter

kubectl create serviceaccount karpenter -n karpenter

kubectl annotate serviceaccount -n karpenter karpenter \
eks.amazonaws.com/role-arn=arn:aws:iam::164063389910:role/karpenter

kubectl describe sa karpenter -n karpenter 

Name:                karpenter
Namespace:           karpenter
Labels:              <none>
Annotations:         eks.amazonaws.com/role-arn: arn:aws:iam::164063389910:role/karpenter
Image pull secrets:  <none>
Mountable secrets:   karpenter-token-xfjx4
Tokens:              karpenter-token-xfjx4
Events:              <none>
```

## Create the EC2 Spot Service Linked Role

This step is only necessary if this is the first time you're using EC2 Spot in this account.

```
aws iam create-service-linked-role --aws-service-name spot.amazonaws.com --profile cloud-nation-development
```

## Install Karpenter Helm Chart

```
helm repo add karpenter https://charts.karpenter.sh
helm repo update
helm upgrade --install karpenter karpenter/karpenter --namespace karpenter \
  --create-namespace --set serviceAccount.create=false --version 0.5.0 \
  --set controller.clusterName=${CLUSTER_NAME} \
  --set controller.clusterEndpoint=$(aws eks describe-cluster --name ${CLUSTER_NAME} --query "cluster.endpoint" --output json --profile cloud-nation-development) \
  --wait # for the defaulting webhook to install before creating a Provisioner
```

## List all Kubernetes objects in Karpenter

```
kubectl get all -n karpenter                                                                                                                         ─╯
NAME                                        READY   STATUS    RESTARTS   AGE
pod/karpenter-controller-66d5cc494b-hgh4d   1/1     Running   0          36s
pod/karpenter-webhook-7b8f7f5988-8p9z4      1/1     Running   0          36s

NAME                        TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/karpenter-metrics   ClusterIP   172.20.107.224   <none>        8080/TCP   37s
service/karpenter-webhook   ClusterIP   172.20.123.232   <none>        443/TCP    37s

NAME                                   READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/karpenter-controller   1/1     1            1           38s
deployment.apps/karpenter-webhook      1/1     1            1           38s

NAME                                              DESIRED   CURRENT   READY   AGE
replicaset.apps/karpenter-controller-66d5cc494b   1         1         1       38s
replicaset.apps/karpenter-webhook-7b8f7f5988      1         1         1       38s
```

## Create the Provisioner

```
cat <<EOF | kubectl apply -f -
apiVersion: karpenter.sh/v1alpha5
kind: Provisioner
metadata:
  name: default
spec:
  requirements:
    - key: karpenter.sh/capacity-type
      operator: In
      values: ["spot"]
  limits:
    resources:
      cpu: 1000
  provider:
    instanceProfile: KarpenterNodeInstanceProfile-${CLUSTER_NAME}
  ttlSecondsAfterEmpty: 30
EOF
```

## First Use

```
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: inflate
spec:
  replicas: 0
  selector:
    matchLabels:
      app: inflate
  template:
    metadata:
      labels:
        app: inflate
    spec:
      terminationGracePeriodSeconds: 0
      containers:
        - name: inflate
          image: public.ecr.aws/eks-distro/kubernetes/pause:3.2
          resources:
            requests:
              cpu: 1
EOF

kubectl scale deployment inflate --replicas 5

kubectl logs -f -n karpenter $(kubectl get pods -n karpenter -l karpenter=controller -o name)
```

## Resources

- https://karpenter.sh/docs/getting-started/
- https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html
- https://docs.aws.amazon.com/eks/latest/userguide/specify-service-account-role.html